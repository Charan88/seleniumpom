package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ViewLeadPage extends ProjectMethods {
	
	public ViewLeadPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.ID,using="viewLead_companyName_sp") WebElement eleCompanyName;
	public ViewLeadPage verifyCompanyName(String data) {
		verifyPartialText(eleCompanyName, data);
		return this;
	}
	

}
