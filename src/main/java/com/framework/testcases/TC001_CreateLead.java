package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_CreateLead extends ProjectMethods {
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testDescription = "Create a Lead in leaftaps";
		testNodes = "Leads";
		author = "Charan";
		category = "smoke";
		dataSheetName = "TC001";
	}

	@Test(dataProvider="fetchData") 
	public void createLead(String userName,String password, String companyName, String firstName, String lastName)
	{
		new LoginPage()
		.enterUsername(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCrmSfa()
		.clickCreteLead()
		.enterCompanyName(companyName)
		.enterFirstName(firstName)
		.enterLastName(lastName)
		.clickCreateLeadButton()
		.verifyCompanyName(companyName);
		
		
		
		
	}
	
}
